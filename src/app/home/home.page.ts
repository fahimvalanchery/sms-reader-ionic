import { Component } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
declare var SMS: any;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  messages: any = [];
  constructor(public androidPermissions: AndroidPermissions) {}

  /**
   * On Page View Loaded
   */
  ionViewWillEnter() {
    this.checkPermission();
  }

  /**
   * Check Permission
   */

  checkPermission() {
    this.androidPermissions
      .checkPermission(this.androidPermissions.PERMISSION.READ_SMS)
      .then(
        success => {
          //if permission granted
          this.ReadSMSList();
        },
        err => {
          this.androidPermissions
            .requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
            .then(
              success => {
                this.ReadSMSList();
              },
              err => {
                alert('cancelled');
              }
            );
        }
      );

    this.androidPermissions.requestPermissions([
      this.androidPermissions.PERMISSION.READ_SMS
    ]);
  }

  /**
   * Read SMS List
   */

  ReadSMSList() {
    let filter = {
      box: 'inbox', // 'inbox' (default), 'sent', 'draft'
      indexFrom: 0, // start from index 0
      maxCount: 20 // count of SMS to return each time
    };

    if (SMS)
      SMS.listSMS(
        filter,
        ListSms => {
          this.messages = ListSms;
        },

        Error => {
          alert(JSON.stringify(Error));
        }
      );
  }
}
